angular.module('myApp')
    .controller('headerCtrl', function ($scope, authToken) {
        $scope.isAuthenticated = authToken.isAuthenticated();
    })