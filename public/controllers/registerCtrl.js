angular.module('myApp')
    .controller('registerCtrl', function ($scope, $http, $rootScope, alert, authToken) {
        $scope.submit = function () {

           var url = '/register';
            var user = {
                email: $scope.email,
                password:$scope.password
            };
            $http.post(url, user)
                .success(function(res){
                    alert('success', 'OK! ', 'Welcome, '+ res.user.email + '!');
                    authToken.setToken(res.token);
                })
                .error(function (err) {
                    alert('warning', 'Opps!', 'Could not register');
                });
        }
    });