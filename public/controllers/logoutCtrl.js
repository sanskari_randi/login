angular.module('myApp')
    .controller('logoutCtrl', function(authToken, $state){
        authToken.removeToken();
        $state.go('home');
    })