angular.module('myApp').config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: '/views/home.html'
        })
        .state('register', {
        url: '/register',
        templateUrl: '/views/register.html',
        controller: 'registerCtrl'
    })
        .state('logout', {
            url: '/logout',
            controller: 'logoutCtrl'
        })
        .state('job', {
            url: '/job',
            templateUrl: '/views/jobs.html',
            controller: 'jobCtrl'
        });
})