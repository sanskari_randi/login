var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var User = require('./app/models/user');
var jwt = require('./app/services/jwt');


var config = require('./config');

mongoose.connect(config.db ,function (err) {
    if(err)
        console.log(err);
     else{
        console.log('connected to Mongolab!!');
    }
});


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

    next();
});

app.use(express.static(__dirname + '/public'));




app.post('/register', function (req, res) {
    var newUser = new User.model({
        email: req.body.email,
        password: req.body.password
    })

    var payload = {
        iss: req.hostname,
        sub: req.body._id
    }

    var token = jwt.encode(payload, 'sshhhh...koihai');
    newUser.save(function (err) {
        res.status(200).send({
            user: newUser.toJSON(),
            token: token
        });
    });

})

console.log(jwt.encode('he', 'secret'));

app.listen(config.PORT);
console.log('Magic happens on port ' + config.PORT);